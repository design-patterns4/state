package org.example.state;

import org.apache.commons.lang3.StringUtils;
import org.example.Context;
import org.example.Device;

/**
 * DocumentChoiceState.
 *
 * @author Tatyana_Dolnikova
 */
public class DocumentChoiceState extends CopyMachineState {

    public DocumentChoiceState() {
        super.message = "Выберите документ.";
        showMessage();
    }

    @Override
    public void pay(Context context, Integer money) {
        showMessage();
    }

    @Override
    public void chooseDevice(Context context, Device device) {
        showMessage();
    }

    @Override
    public void chooseDocument(Context context, String documentName) {
        if (StringUtils.isEmpty(documentName)) {
            showMessage();
            return;
        }
        System.out.println("Выбран документ: " + documentName + ".");
        context.setDocumentName(documentName);
        context.setState(new PrintingState());
    }

    @Override
    public void printDocument(Context context) {
        showMessage();
    }

    @Override
    public void getChange(Context context) {
        showMessage();
    }
}
