package org.example.state;

/**
 * CopyMachine.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class CopyMachineState implements State {

    public String message;

    public CopyMachineState() {
        this.message = "Ошибка.";
    }

    @Override
    public void showMessage() {
        System.out.println(message);
    }
}
