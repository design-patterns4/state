package org.example.state;

import org.example.Context;
import org.example.Device;

/**
 * ReadyToPrint.
 *
 * @author Tatyana_Dolnikova
 */
public class PrintingState extends CopyMachineState {

    public PrintingState() {
        super.message = "Нажмите \"Печать\"";
        showMessage();
    }

    @Override
    public void pay(Context context, Integer money) {
        showMessage();
    }

    @Override
    public void chooseDevice(Context context, Device device) {
        showMessage();
    }

    @Override
    public void chooseDocument(Context context, String documentName) {
        showMessage();
    }

    @Override
    public void printDocument(Context context) {
        context.subtractPrice();
        System.out.println("* * * * * * * * *");
        System.out.println("Печать документа: " + context.getDocumentName());
        System.out.println("* * * * * * * * *");
        context.setState(new ChangeReceivingState());
    }

    @Override
    public void getChange(Context context) {
        showMessage();
    }
}
