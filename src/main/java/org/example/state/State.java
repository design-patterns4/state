package org.example.state;

import org.example.Context;
import org.example.Device;

/**
 * State.
 *
 * @author Tatyana_Dolnikova
 */
public interface State {

    void pay(Context context, Integer money);

    void chooseDevice(Context context, Device device);

    void chooseDocument(Context context, String documentName);

    void printDocument(Context context);

    void getChange(Context context);

    void showMessage();

}
