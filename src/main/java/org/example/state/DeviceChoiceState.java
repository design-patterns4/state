package org.example.state;

import org.example.Context;
import org.example.Device;

/**
 * DeviceChoiceState.
 *
 * @author Tatyana_Dolnikova
 */
public class DeviceChoiceState extends CopyMachineState {

    public DeviceChoiceState() {
        super.message = "Выберите устройство ввода";
        showMessage();
    }

    @Override
    public void pay(Context context, Integer money) {
        Integer currentAccount = context.getAccount() + money;
        context.setAccount(currentAccount);
        System.out.println("На счету " + context.getAccount() + " рублей.");
    }

    @Override
    public void chooseDevice(Context context, Device device) {
        if (device == null) {
            showMessage();
            return;
        }
        System.out.println("Выбран: " + device.name());
        context.setDevice(device);
        context.setState(new DocumentChoiceState());
    }

    @Override
    public void chooseDocument(Context context, String documentName) {
        showMessage();
    }

    @Override
    public void printDocument(Context context) {
        showMessage();
    }

    @Override
    public void getChange(Context context) {
        showMessage();
    }

}
