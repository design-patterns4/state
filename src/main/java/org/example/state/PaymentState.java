package org.example.state;

import org.example.Context;
import org.example.Device;

/**
 * ReadyToPay.
 *
 * @author Tatyana_Dolnikova
 */
public class PaymentState extends CopyMachineState {

    private final static Integer MIN_SUM = 10;

    public PaymentState() {
        super.message = "Для начала печати на счету должно быть не менее " + MIN_SUM + " рублей";
        showMessage();
    }

    @Override
    public void pay(Context context, Integer money) {
        Integer currentAccount = context.getAccount() + money;
        context.setAccount(currentAccount);
        System.out.println("На счету: " + currentAccount + " рублей.");
        if (currentAccount >= MIN_SUM) {
            System.out.println("Выберите устройство ввода.");
            context.setState(new DeviceChoiceState());
            return;
        }
        System.out.println("Минимальная сумма для начала печати: " + MIN_SUM + " рублей.");
    }

    @Override
    public void chooseDevice(Context context, Device device) {
        showMessage();
    }

    @Override
    public void chooseDocument(Context context, String documentName) {
        showMessage();
    }

    @Override
    public void printDocument(Context context) {
        showMessage();
    }

    @Override
    public void getChange(Context context) {
        showMessage();
    }

}
