package org.example.state;

import org.example.Context;
import org.example.Device;

/**
 * ReadyToPay.
 *
 * @author Tatyana_Dolnikova
 */
public class ChangeReceivingState extends CopyMachineState {

    public ChangeReceivingState() {
        super.message = "Заберите сдачу.";
        showMessage();
    }

    @Override
    public void pay(Context context, Integer money) {
        showMessage();
    }

    @Override
    public void chooseDevice(Context context, Device device) {
        showMessage();
    }

    @Override
    public void chooseDocument(Context context, String documentName) {
        showMessage();
    }

    @Override
    public void printDocument(Context context) {
        if (context.subtractPrice()) {
            System.out.println("* * * * * * * * *");
            System.out.println("Печать документа: " + context.getDocumentName());
            System.out.println("* * * * * * * * *");
        } else {
            showMessage();
        }
    }

    @Override
    public void getChange(Context context) {
        Integer change = context.getAccount();
        if (change >= 0) {
            System.out.println("Сдача: " + change + " рублей.");
            context.setAccount(0);
        }
    }
}
