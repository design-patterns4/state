package org.example;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.example.state.CopyMachineState;
import org.example.state.PaymentState;

/**
 * Session.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Context {

    Integer account = 0;
    Device device;
    String documentName;
    CopyMachineState state;

    final static Integer PRICE = 5;

    public Context() {
        this.state = new PaymentState();
    }

    public void pay(Integer money) {
        state.pay(this, money);
    }

    public void chooseDevice(Device device) {
        state.chooseDevice(this, device);
    }

    public void chooseDocument(String documentName) {
        state.chooseDocument(this, documentName);
    }

    public void printDocument() {
        state.printDocument(this);
    }

    public void getChange() {
        state.getChange(this);
    }

    public boolean subtractPrice() {
        int account = this.getAccount() - PRICE;
        if (account < 0) return false;
        this.setAccount(account);
        return true;
    }

}
