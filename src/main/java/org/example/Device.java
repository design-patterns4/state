package org.example;

/**
 * Device.
 *
 * @author Tatyana_Dolnikova
 */
public enum Device {

    USB,
    WIFI

}
