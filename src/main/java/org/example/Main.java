package org.example;

import lombok.var;

/**
 * Main.
 *
 * @author Tatyana_Dolnikova
 */
public class Main {

    public static void main(String[] args) {
        var context = new Context();

        context.pay(5);
        context.printDocument();
        context.pay(5);
        context.pay(2);

        context.printDocument();

        context.chooseDevice(Device.USB);

        context.chooseDocument("doc1");

        context.printDocument();
        context.printDocument();
        context.printDocument();
        context.printDocument();
        context.printDocument();
        context.printDocument();
        context.printDocument();

        context.getChange();
        context.getChange();
    }
}
